# import the kivy stuff
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.config import ConfigParser
from kivy.core.clipboard import Clipboard
import json

# import the master pass stuff
import hashlib

# Get the config
config = ConfigParser()
config.read("myconfig.ini")
config.setdefaults(
    "main",
    {
        "username": "",
        "salt": "cake is a lie azeoiaziheiazhjeiuazyeuizaijbvzgeuiye123456789876543456766678453!!!",
    },
)

# Build the front-end
Builder.load_string(
    """
<MainScreen>:
    GridLayout:
        id: main_grid
        cols: 2

        Label:
            id: website_label
            text: "Website"
        TextInput:
            id: website_field
        Label:
            id: psw_label
            text: "Password"
        TextInput:
            password: True
            id: psw_field

        Button:
            id: generate
            text: "Generate"
            on_press: root.generate()

        Button:
            id: settings
            text: "Settings"
            on_press: app.open_settings()
"""
)

# The main screen of the app (back-end)
class MainScreen(Screen):
    def generate(self):
        #print(self.ids.website_field.text)
        password = (
            config.get("main", "username")
            + config.get("main", "salt")
            + self.ids.website_field.text
            + self.ids.psw_field.text
            + config.get("main", "salt")
        )
        password = hashlib.md5(bytes(password, "utf-8")).hexdigest() + "ABC123!_*"
        #print(password)
        Clipboard.copy(password)
        self.ids.website_field.text = ''
        self.ids.psw_field.text = ''


# Set the screenmanager to switch between screens
sm = ScreenManager()
sm.add_widget(MainScreen(name="main"))

# The main class "App" and the configuration of the setting's panel
class MainApp(App):
    def build(self):
        self.use_kivy_settings = False
        return sm

    def build_settings(self, settings):
        settings.add_json_panel(
            "Settings",
            config,
            data=json.dumps(
                [
                    {
                        "type": "string",
                        "title": "Username",
                        "section": "main",
                        "key": "username",
                    },
                    {
                        "type": "string",
                        "title": "Salt",
                        "desc": "If you change this value, you will need to change it on other devices as well...",
                        "section": "main",
                        "key": "salt",
                    },
                ]
            ),
        )


# Running the app
if __name__ == "__main__":
    MainApp().run()
